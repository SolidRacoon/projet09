from flask import Flask, render_template
import gzip
from io import BytesIO

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/<path:path>')
def serve_file(path):
    file_content = open(path, 'rb').read()
    compressed_content = compress_content(file_content)
    response = app.make_response(compressed_content)
    response.headers['Content-Encoding'] = 'gzip'
    return response

def compress_content(content):
    with BytesIO() as f:
        with gzip.GzipFile(mode='wb', fileobj=f) as gz:
            gz.write(content)
        return f.getvalue()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True, ssl_context=('localhost.pem', 'localhost-key.pem'))

